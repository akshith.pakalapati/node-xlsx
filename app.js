const express = require('express');
const xlsx = require('xlsx');
const _ = require('lodash');
// const bodyParser = require('body-parser');
const files = require('express-fileupload');

const PORT = 3000;
const app = express();

// app.use(bodyParser.urlencoded({ extended: true }));
app.use(files());

const router = express.Router();
var fileName, excelRows;



router.post('/', (req, res) => {
    var fileUpload;
    if (!req.files) {
        console.log("no file found");
        return;
    }
    fileUpload = req.files.fileUpload;
    fileName = fileUpload.name;
    console.log(fileUpload.name);
    var workbook = xlsx.readFile(fileName);
    var sheetName = workbook.SheetNames;
    console.log(sheetName[0]);
    var workSheet = workbook.Sheets[sheetName[0]];
    excelRows = xlsx.utils.sheet_to_row_object_array(workSheet);
    return excelRows;

});

router.get('/', (req, res) => {
    var data = _.find(excelRows, function (o) {
        return !o.Operator;
    });

    let arr = [], collecKey = [];
    _.forEach(data, (Value) => {
        arr.push(Value.CustomerName);

    });
    console.log(arr.length);
    res.json({ data: arr });
});

router.get('/idea', (req, res) => {
    var data = _.filter(excelRows, { 'Operator': 'idea' });
    // console.log(data);
    let arr = [], collecKey = [];
    _.forEach(data, (Value) => {
        arr.push(Value.CustomerName);

    });
    let collection = _.groupBy(arr);
    _.forEach(collection, (Value, key) => {
        collecKey.push({ "name": key, "qty": Value.length });
    });
    res.json({ data: collecKey });
});

router.get('/airtel', (req, res) => {
    var data = _.filter(excelRows, { 'Operator': 'airtel' });
    console.log(data.length);
    let arr = [], collecKey = [];
    _.forEach(data, (Value) => {
        arr.push(Value.CustomerName);

    });
    let collection = _.groupBy(arr);
    _.forEach(collection, (Value, key) => {
        collecKey.push({ "name": key, "qty": Value.length });
    });
    res.json({ data: collecKey });
});

router.get('/jio', (req, res) => {
    var data = _.filter(excelRows, { 'Operator': 'jio' });
    console.log(data.length);
    let arr = [], collecKey = [];
    _.forEach(data, (Value) => {
        arr.push(Value.CustomerName);

    });
    let collection = _.groupBy(arr);
    _.forEach(collection, (Value, key) => {
        collecKey.push({ "name": key, "qty": Value.length });
    });
    res.json({ data: collecKey });
});

router.get('/vodafone', (req, res) => {
    var data = _.filter(excelRows, { 'Operator': 'vodafone' });
    console.log(data.length);
    let arr = [], collecKey = [];
    _.forEach(data, (Value) => {
        arr.push(Value.CustomerName);

    });
    let collection = _.groupBy(arr);
    _.forEach(collection, (Value, key) => {
        collecKey.push({ "name": key, "qty": Value.length });
    });
    res.json({ data: collecKey });
});

app.use('/api', router);

app.listen(PORT, () => {
    console.log(`server is running on the port: ${PORT}`);
})
